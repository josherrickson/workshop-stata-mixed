dyndocs = 01-mixed-model-theory.qmd 02-fitting-mixed-models.qmd \
          03-random-slopes.qmd 04-generalized-linear-mixed-models.qmd


.PHONY:default
default: $(dyndocs)
	quarto render

.PHONY:stata
stata: $(dyndocs)
	@echo > /dev/null

$(dyndocs): %.qmd: %.dyndoc
	/Applications/Stata/StataSE.app/Contents/MacOS/stata-se -b 'dyntext "$<", saving("$@") replace nostop'

.PHONY:open
open:
	@open docs/index.html

.PHONY:preview
preview:
	quarto preview


.PHONY: check-valid-public-site
check-valid-public-site:
	@if [ ! -d "../public-site" ]; then \
		echo "../public-site does not exist."; \
		exit 1; \
	fi
	@if [ ! -d "../public-site/.git" ]; then \
		echo "../public-site is not a Git repository."; \
		exit 1; \
	fi

.PHONY: publicize
publicize: check-valid-public-site
	@echo Syncing to ../public-site/public
	@mkdir -p ../public-site/public/workshop-stata-mixed
	@rsync -rv --delete docs/ ../public-site/public/workshop-stata-mixed
